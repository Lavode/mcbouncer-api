# Copyright (C) 2012 Michael Senn "Morrolan"
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# API documentation: http://mcbouncer.com/api/
import urllib.request
import urllib.parse
import json
import logging
import re

# Todo: Seemingly it's not Python-ish to check whether a parameter is of the right type, one should do so with Error handling instead.
# TODO: Right now it seems that the API ignores the "Bans per page" and "page" settings entirely. Will have to ask the peeps in IRC 'bout that.
# -> 09.06.2012 -> According to Deaygo on IRC he noticed this too.
# Todo: The Ban class should have a import_from_dictionary() function themselves, which I then call in the playerinfo's improt function.


class APIManager(object):
    APIGETURL = "http://www.mcbouncer.com/api/{action}/{apikey}/{params}"

    _ValidNoteTypes =  {
                    "local": 0,
                    "global": 1,
                    }
    _Regex_APIKey = "[0-fA-F]{32}"

    def __init__(self):
        """Initializes the API manager."""
        self._APIGETURL = APIManager.APIGETURL
        self._APIKey = None
        self._cRegex_APIKey = re.compile(APIManager._Regex_APIKey)
        logging.info("APIManager object initialized.")


    def _get_api_key(self):
        return self._APIKey

    def _set_api_key(self, value):
        if not self._cRegex_APIKey.match(str(value)):
            logging.warning("API key might be invalid: " + value)
        self._APIKey = str(value)
        logging.debug("APIKey set to: " + self._APIKey)
        #self.__urlAPI = MCBouncerAPIManager.urlAPI.format(apikey=self.__APIKey)

    APIKey = property(_get_api_key, _set_api_key)


    def add_player_ban(self, issuer, player, reason):
        """Bans the specified player.

        Parameters:
        issuer -- The admin who issued the ban.
        player -- The player whom to ban.
        reason -- The ban reason.

        Returns:
        str -- The player who got banned.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """

        action = "addBan"
        params = [issuer, player, reason]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return input["username"]

    def remove_player_ban(self, player):
        """Unbans the specified player.

        Parameters:
        player -- The player whose ban to remove.

        Returns:
        str -- The player who got unbanned.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """

        action = "removeBan"
        params = [player]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            if input["error"] == "User not banned":
                raise APIError(url, input, "User was not banned.")
            else:
                raise APIError(url, input)

        return input["username"]

    def get_player_bans(self, player, page=0, bansperpage=10):
        """Returns a player's bans.

        Parameters:
        player -- The player whose bans to query.
        page=0 -- The index (zero-based) of the page with the ip's bans.
        bansperpage=10 -- The amount of bans per page to display.

        Returns:
        dict -- {"Bancount": int, "Banlist": [{"Player": str, "Reason": str, "Time": "YYYY-MM-DD HH:MM:SS", "Issuer": str, "Server": str}], "Page": int}

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """

        action = "getBans"
        params = [player, page, bansperpage]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        # Parse the dictionary we got from the API and put it into a nicer one.
        result = {"Bancount": None, "Page": None, "Banlist": []}

        for key in input:
            if key == "totalcount":
                result["Bancount"] = input["totalcount"]
            elif key == "page":
                result["Page"] = input["page"]
            elif key == "data":
                banlist = []
                for banentry in input["data"]:
                    Ban = {}
                    for banentry_key in banentry:
                        if banentry_key == "username":
                            Ban["Player"] = banentry[banentry_key]
                        elif banentry_key == "issuer":
                            Ban["Issuer"] = banentry[banentry_key]
                        elif banentry_key == "reason":
                            Ban["Reason"] = banentry[banentry_key]
                        elif banentry_key == "server":
                            Ban["Server"] = banentry[banentry_key]
                        elif banentry_key == "time":
                            Ban["Time"] = banentry[banentry_key]
                            # This is where the individual ban entry has been parsed.
                    banlist.append(Ban)
                    # This is where the whole banlist has been parsed.
                result["Banlist"].extend(banlist)

        # And here the whole parsing of the API query is done.
        return result

    def get_player_ban_count(self, player):
        """Returns a player's number of bans.

        Parameters:
        player -- The player whose amount of bans to query for.

        Returns:
        int -- The amount of bans the player has.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "getBanCount"
        params = [player]

        url = self._construct_url(action, params)
        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return input["totalcount"]

    def get_player_ban_reason(self, player):
        """Returns a player's ban reason. (Regarding a ban on your server / a subscribed server.)

        Parameters:
        player -- The player whose ban reason to look up.

        Returns:
        dict -- {"isBanned": bool, "Reason": str}

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "getBanReason"
        params = [player]

        url = self._construct_url(action, params)
        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        result = {"isBanned": input["is_banned"], "Reason": input["reason"]}
        return result

    def get_player_ban_details(self, player):
        """Returns a player's ban details. (Regarding a ban on your server / a subscribed server.)

        Parameters:
        player -- The player whose ban details to look up.

        Returns:
        dict -- {"isBanned": bool, "Reason": str, "Time": str, "Issuer": str, "SubscribedServer": str}

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        # TODO: Is there anything similar for IP bans?

        action = "getBanDetails"
        params = [player]

        url = self._construct_url(action, params)
        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        # If the user wasn't banned, the dictionary will not contain all the other keys.
        if input["is_banned"]:
            result = {"isBanned": input["is_banned"], "Reason": input["reason"], "Time": input["time"], "Issuer": input["issuer"]}
        else:
            result = {"isBanned": input["is_banned"], "Reason": None, "Time": None, "Issuer": None}

        # Seemingly this key/value pair is only in the dictionary if the ban actually originates from a subscribed server.
        try:
            result["SubscribedServer"] = input["subscribed_server"]
        except KeyError:
            pass

        return result


    def add_ip_ban(self, issuer, ip, reason):
        """Bans the specified IP.

        Parameters:
        issuer -- The admin who issued the ban.
        ip -- The ip address which to ban.
        reason -- The ban reason.

        Returns:
        str -- The IP address which got banned.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """

        action = "addIPBan"
        params = [issuer, ip, reason]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return input["ip"]

    def remove_ip_ban(self, ip):
        """Unbans the specified IP.

        Parameters:
        ip -- The IP which should get unbanned.

        Returns:
        str -- The IP address which got unbanned.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "removeIPBan"
        params = [ip]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            if input["error"] == "IP not banned":
                raise APIError(url, input, "IP was not banned.")
            else:
                raise APIError(url, input)

        return input["ip"]

    def get_ip_bans(self, ip, page=0, bansperpage=10):
        """Returns an IP's bans.

        Parameters:
        ip -- The IP whose bans to look up.
        page=0 -- The index (zero-based) of the page with the IP's bans.
        bansperpage=10 -- The amount of bans per page to display.

        Returns:
        dict -- {"Bancount": int, "Banlist": [{"IP": str, "Reason": str, "Issuer": str, "Server": str, "Time": str}], "Page": int}

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "getIPBans"
        params = [ip, page, bansperpage]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        # Parse the dictionary we got from the API and put it into a nicer one.
        result = {"Bancount": None, "Page": None, "Banlist": []}

        for key in input:
            if key == "totalcount":
                result["Bancount"] = input["totalcount"]
            elif key == "page":
                result["Page"] = input["page"]
            elif key == "data":
                banlist = []
                for banentry in input["data"]:
                    ban = {}
                    for banentry_key in banentry:
                        if banentry_key == "ip":
                            ban["IP"] = banentry[banentry_key]
                        elif banentry_key == "issuer":
                            ban["Issuer"] = banentry[banentry_key]
                        elif banentry_key == "reason":
                            ban["Reason"] = banentry[banentry_key]
                        elif banentry_key == "server":
                            ban["Server"] = banentry[banentry_key]
                        elif banentry_key == "time":
                            ban["Time"] = banentry[banentry_key]
                        # This is where the individual ban entry has been parsed.
                    banlist.append(ban)
                    # This is where the whole banlist has been parsed.
                result["Banlist"].extend(banlist)

        return result

    def get_ip_ban_count(self, ip):
        """Returns an IP's number of bans.

        Parameters:
        ip -- The IP whose amount of bans to query for.

        Returns:
        int -- The amount of bans the IP has.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "getIPBanCount"
        params = [ip]

        url = self._construct_url(action, params)
        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return input["totalcount"]

    def get_ip_ban_reason(self, ip):
        """Returns an IP's ban reason. (Regarding a ban on your server / a subscribed server)

        Parameters:
        ip -- The IP whose banreason to look up.

        Returns:
        dict -- {"isBanned": bool, "Reason": str}

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "getIPBanReason"
        params = [ip]

        url = self._construct_url(action, params)
        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        result = {"isBanned": input["is_banned"], "Reason": input["reason"]}
        return result


    def add_note(self, issuer, player, note, type=0):
        """Adds a note to the specified user.

        Parameters:
        issuer -- The admin who added the note.
        player -- The player whom to add the note to.
        note -- The text of the note.

        Returns:
        int -- The ID of the note which was added.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if an invalid note type was supplied.

        """
        if type in APIManager._ValidNoteTypes.values():
            pass
        elif type in APIManager._ValidNoteTypes:
            type = APIManager._ValidNoteTypes[type]
        else:
            raise ValueError("Invalid note type supplied!")

        if type == 0:
            self.add_local_note(issuer, player, note)
        elif type == 1:
            self.add_global_note(issuer, player, note)

    def add_local_note(self, issuer, player, note):
        """Adds a local note to the specified user.

        Parameters:
        issuer -- The admin who added the note.
        player -- The player whom to add the note to.
        note -- The text of the note.

        Returns:
        int -- The ID of the note which was added.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """
        # TODO: Right now there is, theoretically an addNote as well as an addGlobalNote function, but both lead to the same, namely a non-global note.
        # According to Deaygo on IRC this feature isn't fully implemented yet, which is the reason for the API docs not saying anything yet.
        # As for the source on the addGlobalNote: https://github.com/pdaian/MCBouncer/blob/master/src/main/java/com/mcbouncer/api/MCBouncerAPI.java (the code jumps around a lot, but I think I read it right)

        action = "addNote"
        params = [issuer, player, note]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return int(input["note_id"])

    def add_global_note(self, issuer, player, note):
        """Adds a global note to the specified user.

        Parameters:
        issuer -- The admin who added the note.
        player -- The player whom to add the note to.
        note -- The text of the note.

        Returns:
        int -- The ID of the note which was added.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        
        """
        # TODO: As soon as it's clear how global/non-global notes get handled API-wise I'll have to change add_global_note as well as add_local_note, and possibly the docstrings too.
        # As of today, 08.06.2012 this does the same as add_local_note.

        action = "addGlobalNote"
        params = [issuer, player, note]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return int(input["note_id"])

    def remove_note(self, noteid):
        """Removes the specified note.

        Parameters:
        noteid -- the id of the note to remove

        Returns:
        int -- The ID of the note which was removed.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "removeNote"
        params = [noteid]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return input["note_id"]

    def get_notes(self, player, page=0, notesperpage=10):
        """Returns a user's notes.

        Parameters:
        player -- the player whose notes to fetch.
        page -- (optional = 0) the index (zero-based) of the page with the IP's bans.
        bansperpage -- (optional = 10) the amount of bans per page to display.

        Returns:
        dict -- {"Notecount": int, "Notelist": [{"isGlobal": bool, "Server": str, "Note": str, "Player": str, "Time": str, "NoteID": int}], "Page": int}

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "getNotes"
        params = [player, page, notesperpage]
        url = self._construct_url(action, params)

        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        # Parse the dictionary we got from the API and put it into a nicer one.
        result = {}

        for node in input:
            if node == "totalcount":
                result["Notecount"] = input["totalcount"]

            elif node == "data":
                notelist = []
                for noteentry in input["data"]:
                    note = {}
                    for noteentry_detail in noteentry:
                        if noteentry_detail == "username":
                            note["Player"] = noteentry["username"]
                        elif noteentry_detail == "global":
                            note["isGlobal"] = noteentry["global"]
                        elif noteentry_detail == "server":
                            note["Server"] = noteentry["server"]
                        elif noteentry_detail == "note":
                            note["Note"] = noteentry["note"]
                        elif noteentry_detail == "time":
                            note["Time"] = noteentry["time"]
                        elif noteentry_detail == "noteid":
                            note["NoteID"] = int(noteentry["noteid"])
                        elif noteentry_detail == "issuer":
                            note["Issuer"] = noteentry["issuer"]
                    # Here the individual note entry has been parsed.
                    notelist.append(note)
                # Here all the note entries have been parsed.
                result["Notelist"] = notelist
            elif node == "page":
                result["Page"] = input["page"]

        return result

    def get_note_count(self, player):
        """Queries the number of a player's notes.

        Parameters:
        player -- The player whose notes to count.

        Returns:
        integer -- Contains the number of notes the player has.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "getNoteCount"
        params = [player]

        url = self._construct_url(action, params)
        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return input["totalcount"]


    def update_user(self, player, ip):
        """Should be sent whenever a user connects.

        Parameters:
        player -- The player who connected to the server.
        ip -- The IP of the player who connected to the server.

        Returns:
        string -- The username of the player whom you supplied.

        Exceptions:
        APIError -- Will be raised if the API call failed.

        """

        action = "updateUser"
        params = [player, ip]

        url = self._construct_url(action, params)
        input = self._do_api_request(url)

        if not input["success"]:
            raise APIError(url, input)

        return input["username"]


    def _construct_url(self, action, params):
        logging.debug("Constructing URL with:")
        logging.debug("URL:" + self._APIGETURL)
        logging.debug("Action:" + action)
        logging.debug("Parameters:" + str(params))

        # The following line will, for example, transform ["Morrolan","Morrobot","Testing"] into "Morrolan/Morrobot/Testing".
        paramlist = "/".join(str(param) for param in params)

        # url-encoding the list of parameters.
        paramlist = urllib.parse.quote(paramlist)

        url = self._APIGETURL.format(apikey=self._APIKey, action=action, params=paramlist)
        logging.debug("Resulting URL:" + url)

        return url

    def _do_api_request(self, url):

        response = urllib.request.urlopen(url)

        result = response.read()
        result = result.decode("utf-8")
        result = json.loads(result)

        return result


class PlayerInfo(object):
    def __init__(self, player=None, ip=None):
        # Todo: Notecount might not be correct anymore if, for some reason, someone would collect notes from multiple sources inside one PlayerInfo object.
        # Not sure whether Notecount should just return Notelist.count() or whether it should continue to contain the one from the API response. The same applies to Bancount.
        self.Player = player
        self.IP = ip
        self.Bancount = None
        self.Notecount = None
        self.Banlist = []
        self.Notelist = []

    def __str__(self, separator="------"):
        """Returns a print-ready representation of the player's information.

        Parameters:
        separator(="------") -- The separator which is used to separate individual ban entries.

        Returns:
        str -- Print-ready representation of the information regarding the player.
        Exceptions:

        """
        ban_string_list = []
        note_string_list = []

        info_list = [
            "Player: " + str(self.Player),
            "IP: " + str(self.IP),
            "Bancount: " + str(self.Bancount),
            "Notecount: " + str(self.Notecount),
            ]
        info_string = "\n".join(info_list)

        for ban in self.Banlist:
            ban_string_list.append(str(ban))
        ban_string = ("\n" + separator + "\n").join(ban_string_list)

        for note in self.Notelist:
            note_string_list.append(str(note))
        note_string = ("\n" + separator + "\n").join(note_string_list)

        return ("\n" + separator*3 + "\n").join([info_string, ban_string, note_string])

    def import_bans_from_dictionary(self, d, override=False):
        """Imports ban details from a dictionary.

        Parameters:
        d -- The dictionary containing the ban details.
        override -- Whether to override the current ban list or append to it.

        Returns:

        Exceptions:

        """
        if override:
            self.Banlist = []

        try:
            self.Bancount = d["Bancount"]

            for banentry in d["Banlist"]:
                ban = Ban()
                ban.import_from_dictionary(banentry)
                self.Banlist.append(ban)

                # If either Player or IP wasn't set while initializing the PlayerInfo object it'll grab the one from the ban entry.
                if self.Player is None and "Player" in banentry:
                    self.Player = banentry["Player"]
                if self.IP is None and "IP" in banentry:
                    self.IP = banentry["IP"]

        except KeyError:
            logging.warning("\n".join(["PlayerInfo/import_bans_from_dictionary", "A dictionary key was missing, the result will be incomplete!", str(d)]))

    def import_notes_from_dictionary(self, d, override=False):
        """Imports note details from a dictionary.

        Parameters:
        d -- The dictionary containing the note details.
        override -- Whether to override the current note list or append to it.

        Returns:

        Exceptions:

        """
        if override:
            self.Notelist = []

        try:
            self.Notecount = d["Notecount"]

            for noteentry in d["Notelist"]:
                note = Note()
                note.import_from_dictionary(noteentry)
                self.Notelist.append(note)

                # If either Player or IP wasn't set while initializing the PlayerInfo object it'll grab the one from the ban entry.
                if self.Player is None and "Player" in noteentry:
                    self.Player = noteentry["Player"]
                if self.IP is None and "IP" in noteentry:
                    self.IP = noteentry["IP"]

        except KeyError:
            logging.warning("\n".join(["PlayerInfo/import_bans_from_dictionary", "A dictionary key was missing, the result will be incomplete!", str(d)]))

    def clear(self):
        """Resets every attribute and list of the PlayerInfo object.

        Parameters:

        Returns:

        Exceptions:

        """

        self.Player = None
        self.IP = None
        self.Bancount = None
        self.Notecount = None
        self.Banlist = []
        self.Notelist = []


class Ban(object):
    def __init__(self):
        self.Player = None
        self.IP = None
        self.Issuer = None
        self.Reason = None
        self.Server = None
        self.Time = None

    def __str__(self):
        l =  [
        "Player: " + str(self.Player),
        "IP: " + str(self.IP),
        "Issuer: " + str(self.Issuer),
        "Reason: " + str(self.Reason),
        "Server: " + str(self.Server),
        "Time: " + str(self.Time)
        ]

        return "\n".join(l)

    def import_from_dictionary(self, d):
        """Imports ban details from a dictionary.

        Parameters:
        d -- The dictionary containing the ban details.

        Returns:

        Exceptions:

        """
        try:
            self.Issuer = d["Issuer"]
            self.Reason = d["Reason"]
            self.Server = d["Server"]
            self.Time = d["Time"]
            if "Player" in d:
                self.Player = d["Player"]
            elif "IP" in d:
                self.IP = d["IP"]
        except KeyError:
            logging.warning("\n".join(["Ban/import_from_dictionary", "A dictionary key was missing, the result will be incomplete!", str(d)]))



class Note(object):
    def __init__(self):
        self.Player = None
        self.Issuer = None
        self.isGlobal = None
        self.Server = None
        self.Note = None
        self.Time = None
        self.NoteID = None

    def __str__(self):
        l =  [
            "Player: " + str(self.Player),
            "Issuer: " + str(self.Issuer),
            "Is Global: " + str(self.isGlobal),
            "Note: " + str(self.Note),
            "Server: " + str(self.Server),
            "Time: " + str(self.Time),
            "Note ID: " + str(self.NoteID)
            ]

        return "\n".join(l)

    def import_from_dictionary(self, d):
        try:
            self.Player = d["Player"]
            self.Issuer = d["Issuer"]
            self.isGlobal = d["isGlobal"]
            self.Server = d["Server"]
            self.Note = d["Note"]
            self.Time = d["Time"]
            self.NoteID = d["NoteID"]
        except KeyError:
            logging.warning("\n".join(["Note/import_from_dictionary", "A dictionary key was missing, the result will be incomplete!", str(d)]))
        
class APIError(Exception):
    def __init__(self, query, result, message=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.Query = query
        self.Result = result
        self.Message = message

