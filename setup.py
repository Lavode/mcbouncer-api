from distutils.core import setup

setup(
    name = "MCBouncer-API library",
    author = "Michael Senn",
    author_email = "morrolan.minecraft@gmail.com",
    py_modules = ["mcbouncerapi"],
    description = "Small library to access MCBouncer.com's API",
    url = "https://bitbucket.org/Lavode/mcbouncer-api",
    scripts = ["example_usage"]
)